"""
The B{0solve} command-line interface.

This code is here, rather than in B{0solve} itself, simply so that it gets byte-compiled at
install time.
"""

from zeroinstall import _
import os, sys
import logging
from optparse import OptionParser

from zeroinstall import SafeException
from zeroinstall.support import tasks
from zeroinstall.injector import model, selections, config, iface_cache
from zeroinstall.injector.requirements import Requirements
from zeroinstall.injector.driver import Driver
from zeroinstall.injector.handler import BatchHandler

def main(command_args):
	"""Act as if 0solve was run with the given arguments.
	@arg command_args: array of arguments (e.g. C{sys.argv[1:]})
	@type command_args: [str]
	"""
	parser = OptionParser(usage=_("usage: %prog [options] interface [args]"))
	parser.add_option("", "--before", help=_("choose a version before this"), metavar='VERSION')
	parser.add_option("", "--command", help=_("command to select"), metavar='COMMAND')
	parser.add_option("", "--cpu", help=_("target CPU type"), metavar='CPU')
	parser.add_option("", "--message", help=_("message to display when interacting with user"))
	parser.add_option("", "--not-before", help=_("minimum version to choose"), metavar='VERSION')
	parser.add_option("", "--os", help=_("target operation system type"), metavar='OS')
	parser.add_option("-o", "--offline", help=_("try to avoid using the network"), action='store_true')
	parser.add_option("-r", "--refresh", help=_("refresh all used interfaces"), action='store_true')
	parser.add_option("-s", "--source", help=_("select source code"), action='store_true')
	parser.add_option("", "--with-store", help=_("add an implementation cache"), action='append', metavar='DIR')
	parser.add_option("-v", "--verbose", help=_("more verbose output"), action='count')

	(options, args) = parser.parse_args(command_args)

	if options.verbose:
		logger = logging.getLogger()
		if options.verbose == 1:
			logger.setLevel(logging.INFO)
		else:
			logger.setLevel(logging.DEBUG)
		import zeroinstall
		logging.info(_("Running 0install %(version)s %(args)s; Python %(python_version)s"), {'version': zeroinstall.version, 'args': repr(command_args), 'python_version': sys.version})

	if len(args) != 1:
		logging.error("Incorrect number of aruments")
		sys.exit(1)

	if options.with_store:
		from zeroinstall import zerostore
		for x in options.with_store:
			iface_cache.stores.stores.append(zerostore.Store(os.path.abspath(x)))

	try:
		iface_uri = model.canonical_iface_uri(args[0])
		(sels, stale_feeds) = get_selections(config.load_config(handler = BatchHandler()), options, iface_uri)
		show_xml(sels)
		if stale_feeds:
			sys.stdout.write('<!-- STALE_FEEDS -->\n')

	except SafeException, ex:
		logging.error(unicode(ex))
		sys.exit(1)

def get_selections(config, options, iface_uri):
	"""Get selections for iface_uri, according to the options passed.
	Will switch to GUI mode if necessary.
	@param options: options from OptionParser
	@param iface_uri: canonical URI of the interface
	@return: the selected versions, or None if the user cancels
	@rtype: L{selections.Selections} | None
	"""
	if options.offline:
		config.network_use = model.network_offline

	iface_cache = config.iface_cache

	# Try to load it as a feed. If it is a feed, it'll get cached. If not, it's a
	# selections document and we return immediately.
	maybe_selections = iface_cache.get_feed(iface_uri, selections_ok = True)
	if isinstance(maybe_selections, selections.Selections):
		return maybe_selections

	requirements = Requirements(iface_uri)
	requirements.parse_options(options)
	driver = Driver(requirements = requirements, config = config)

	downloaded = driver.solve_and_download_impls(refresh = options.refresh, select_only = True)
	if downloaded:
		tasks.wait_for_blocker(downloaded)

	stale_feeds = [feed for feed in driver.solver.feeds_used if
			not os.path.isabs(feed) and			# Ignore local feeds (note: file might be missing too)
			not feed.startswith('distribution:') and	# Ignore (memory-only) PackageKit feeds
			iface_cache.is_stale(iface_cache.get_feed(feed), config.freshness)]

	return (driver.solver.selections, stale_feeds)

def show_xml(sels):
	doc = sels.toDOM()
	doc.writexml(sys.stdout)
	sys.stdout.write('\n')
