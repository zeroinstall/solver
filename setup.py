from distutils.core import setup
import py2exe
import sys, pywintypes, pythoncom, win32api
import zeroinstall

try:
# if this doesn't work, try import modulefinder
    import py2exe.mf as modulefinder
    import win32com
    for p in win32com.__path__[1:]:
        modulefinder.AddPackagePath("win32com", p)
    for extra in ["win32com.shell"]: #,"win32com.mapi"
        __import__(extra)
        m = sys.modules[extra]
        for p in m.__path__[1:]:
            modulefinder.AddPackagePath(extra, p)
except ImportError:
    # no build path setup, no worries.
    pass

setup(name="zeroinstall-solver",
      version=zeroinstall.version,
      description="The Zero Install Solver (0solve)",
      author="Thomas Leonard, Bastian Eicher",
      author_email="zero-install-devel@lists.sourceforge.net",
      url="http://0install.net",
      data_files = [('', ['zeroinstall/injector/cacert.pem'])],
      license='LGPL',
      options={
        "py2exe":{
          "unbuffered": True,
          #"bundle_files": 1,
          "optimize": 2,
          "excludes": ["adodbapi", "doctest", "pdb", "bdb", "pythonwin", "isapi", "msilib", "json", "email", "hotshot"],
		  "dll_excludes": [ "mswsock.dll", "powrprof.dll", "KERNELBASE.dll", "API-MS-Win-Core-LocalRegistry-L1-1-0.dll", "API-MS-Win-Core-ProcessThreads-L1-1-0.dll", "API-MS-Win-Security-Base-L1-1-0.dll" ]
        },
      },
      #zipfile = None,
      console=['0solve'])
